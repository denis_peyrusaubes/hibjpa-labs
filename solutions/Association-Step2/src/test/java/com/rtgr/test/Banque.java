package com.rtgr.test;

import static org.junit.Assert.assertNotNull;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;

import org.junit.Assert;
import org.junit.Test;

import com.rtgr.bo.Agence;
import com.rtgr.bo.Client;
import com.rtgr.bo.CompteInterne;
import com.rtgr.bo.Conseiller;
import com.rtgr.bo.Operation;
import com.rtgr.utils.JPAUtil;

public class Banque {

	private Long agenceId = null;

	@Test
	public void testCreateBanque2() {
		EntityManager entityManager = null;

		try {
			entityManager = JPAUtil.getEntityManager();
			entityManager.getTransaction().begin();
			Client client1 = new Client("Winston", "Charlie");
			Client client2 = new Client("Perry", "Katty");
			Client client3 = new Client("Aznavour", "Charles");

			Agence a = new Agence("Agence des peupliers");
			a.addClient(client1);
			a.addClient(client2);
			a.addClient(client3);
			a.addConseiller(createConseiller());
			a.addConseiller(createConseiller());
			a.addConseiller(createConseiller());
			a.addConseiller(createConseiller());

			CompteInterne ci1 = createCompteInterne("Compte Courant");
			CompteInterne ci2 = createCompteInterne("Compte Epargne");
			CompteInterne ci3 = createCompteInterne("Compte Joint");
			CompteInterne ci4 = createCompteInterne("Compte Epargne");
			CompteInterne ci5 = createCompteInterne("Compte Courant");
			CompteInterne ci6 = createCompteInterne("Compte Courant");

			entityManager.persist(ci1);
			entityManager.persist(ci2);
			entityManager.persist(ci3);
			entityManager.persist(ci4);
			entityManager.persist(ci5);
			entityManager.persist(ci6);

			entityManager.persist(a);
			assertNotNull(a);
			assertNotNull(a.getId());
			agenceId = a.getId();
			System.out.println("Id's Agence : " + agenceId);

			entityManager.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
			entityManager.getTransaction().rollback();
			Assert.fail();
		} finally {
			entityManager.close();
		}
	}

	@Test
	public void testEAGERvsLAZY() {
		// Ce test doit être démarré avec une base de données populée par testCreateBanque2.
		// La génératio nautomatique du schéma doit donc être désactivé dans persistence.xml
		// L'attribut agenceId doit avoir été affecté avec une valeur conforme dans la base de données.
		EntityManager entityManager = null;

		try {
			entityManager = JPAUtil.getEntityManager();
			entityManager.getTransaction().begin();

			Agence a = findAgenceById(agenceId);
			assertNotNull(a);
			assertNotNull(a.getConseillers());
			assert(a.getConseillers().isEmpty()==false);
			assert(a.getConseillers().size() > 0);

			System.out.println(a.getConseillers().get(0).getNom());

			entityManager.getTransaction().commit();
		} catch (Exception e) {
			
			e.printStackTrace();
			entityManager.getTransaction().rollback();
			Assert.fail();
		} finally {
			entityManager.close();
		}
	}

	// STEP3
	/*
	 * public void testCreateBanque3() { EntityManager entityManager = null;
	 * 
	 * try { entityManager = JPAUtil.getEntityManager();
	 * entityManager.getTransaction().begin();
	 * 
	 * Client client1 = new Client("Winston", "Charlie"); Client client2 = new
	 * Client("Perry", "Katty"); Client client3 = new Client("Aznavour",
	 * "Charles");
	 * 
	 * Agence a = new Agence("Agence des peupliers"); a.addClient(client1);
	 * a.addClient(client2); a.addClient(client3);
	 * a.addConseiller(createConseiller()); a.addConseiller(createConseiller());
	 * a.addConseiller(createConseiller()); a.addConseiller(createConseiller());
	 * 
	 * CompteInterne ci1 = createCompteInterne("Compte Courant");
	 * 
	 * CompteInterne ci2 = createCompteInterne("Compte Epargne");
	 * 
	 * CompteInterne ci3 = createCompteInterne("Compte Joint");
	 * 
	 * client1.addCompteExterne(createCompteExterne());
	 * client1.addCompteExterne(createCompteExterne());
	 * client1.addCompteExterne(createCompteExterne());
	 * 
	 * client2.addCompteExterne(createCompteExterne());
	 * client2.addCompteExterne(createCompteExterne());
	 * client2.addCompteExterne(createCompteExterne());
	 * client2.addCompteExterne(createCompteExterne());
	 * 
	 * client3.addCompteExterne(createCompteExterne());
	 * client3.addCompteExterne(createCompteExterne());
	 * 
	 * CompteInterne ci4 = createCompteInterne("Compte Epargne");
	 * 
	 * CompteInterne ci5 = createCompteInterne("Compte Courant");
	 * 
	 * CompteInterne ci6 = createCompteInterne("Compte Courant");
	 * 
	 * entityManager.persist(ci1); entityManager.persist(ci2);
	 * entityManager.persist(ci3); entityManager.persist(ci4);
	 * entityManager.persist(ci5); entityManager.persist(ci6);
	 * 
	 * entityManager.persist(a); entityManager.getTransaction().commit(); }
	 * catch (Exception e) { e.printStackTrace();
	 * entityManager.getTransaction().rollback(); Assert.fail(); } finally {
	 * entityManager.close(); } }
	 */

	// STEP 4
	/*
	 * public void testCreateBanque4() { EntityManager entityManager = null;
	 * 
	 * try { entityManager = JPAUtil.getEntityManager();
	 * entityManager.getTransaction().begin();
	 * 
	 * Client client1 = new Client("Winston", "Charlie"); Client client2 = new
	 * Client("Perry", "Katty"); Client client3 = new Client("Aznavour",
	 * "Charles");
	 * 
	 * client1.addCompteExterne(createCompteExterne());
	 * client1.addCompteExterne(createCompteExterne());
	 * client1.addCompteExterne(createCompteExterne());
	 * 
	 * client2.addCompteExterne(createCompteExterne());
	 * client2.addCompteExterne(createCompteExterne());
	 * client2.addCompteExterne(createCompteExterne());
	 * client2.addCompteExterne(createCompteExterne());
	 * 
	 * client3.addCompteExterne(createCompteExterne());
	 * client3.addCompteExterne(createCompteExterne());
	 * 
	 * CompteInterne ci1 = createCompteInterne("Compte Courant");
	 * client1.addCompteInterne(ci1);
	 * 
	 * CompteInterne ci2 = createCompteInterne("Compte Epargne");
	 * client1.addCompteInterne(ci2);
	 * 
	 * CompteInterne ci3 = createCompteInterne("Compte Joint");
	 * client2.addCompteInterne(ci3); client3.addCompteInterne(ci3);
	 * 
	 * CompteInterne ci4 = createCompteInterne("Compte Epargne");
	 * client3.addCompteInterne(ci4);
	 * 
	 * CompteInterne ci5 = createCompteInterne("Compte Courant");
	 * client3.addCompteInterne(ci5);
	 * 
	 * CompteInterne ci6 = createCompteInterne("Compte Courant");
	 * client2.addCompteInterne(ci6);
	 * 
	 * Agence a = new Agence("Agence des peupliers"); a.addClient(client1);
	 * a.addClient(client2); a.addClient(client3);
	 * a.addConseiller(createConseiller()); a.addConseiller(createConseiller());
	 * a.addConseiller(createConseiller()); a.addConseiller(createConseiller());
	 * 
	 * entityManager.persist(a); entityManager.getTransaction().commit(); }
	 * catch (Exception e) { e.printStackTrace();
	 * entityManager.getTransaction().rollback(); Assert.fail(); } finally {
	 * entityManager.close(); }
	 * 
	 * }
	 */

	private int getNumber() {

		return ((int) (Math.random() * 100));
	}

	private Date createDate() {
		Date result;
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(GregorianCalendar.DAY_OF_YEAR, ((int) (Math.random() * 365) + 1));
		gc.set(GregorianCalendar.YEAR, 2000 + (((int) (Math.random() * 8) + 1)));
		result = gc.getTime();
		return result;
	}

	private Conseiller createConseiller() {
		return new Conseiller("nom" + getNumber(), "prenom" + getNumber());
	}

	private CompteInterne createCompteInterne(String description) {
		CompteInterne result = new CompteInterne("num" + getNumber(), description, getNumber() * 20);
		int n = getNumber();
		Operation o;
		for (int i = 0; i < n; i++) {
			o = new Operation(createDate(), "Boucher", 12.78);
			result.addOperation(o);
		}

		return result;
	}

	private Agence findAgenceById(long id) {
		EntityManager manager = JPAUtil.getEntityManager();
		Agence a = manager.find(Agence.class, id);
		return a;
	}
}
