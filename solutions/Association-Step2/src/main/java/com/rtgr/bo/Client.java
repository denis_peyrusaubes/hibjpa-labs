package com.rtgr.bo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Client{

	private Long id;
	private String nom;
	private String prenom;
	
	private List<CompteInterne> compteInternes = new ArrayList<CompteInterne>();

	private Agence agence;

	public Client(String nom, String prenom) {
		this.nom = nom;
		this.prenom = prenom;
	}

	protected Client() {
	}

	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public void setCompteInternes(List<CompteInterne> compteInternes) {
		this.compteInternes = compteInternes;
	}

	public void addCompteInterne(CompteInterne ci) {
		this.compteInternes.add(ci);
		ci.setTitulaire(this);
	}

	
	@ManyToOne
	@JoinColumn(name = "FK_AGENCE")
	public Agence getAgence() {
		return agence;
	}

	public void setAgence(Agence agence) {
		this.agence = agence;
	}
	
	@OneToMany(mappedBy = "titulaire", cascade = { CascadeType.ALL })
	protected List<CompteInterne> getCompteInternes() {
		return compteInternes;
	}

	

}
