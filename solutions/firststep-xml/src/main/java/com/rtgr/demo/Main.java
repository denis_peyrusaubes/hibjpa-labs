package com.rtgr.demo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.rtgr.bo.CompteInterne;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("RTGR_PU");

		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();


		CompteInterne c1 = new CompteInterne();
		c1.setDescription("nouveau Compte");
		c1.setSolde(40000);
		
		em.persist(c1);
		

		em.getTransaction().commit();
		em.close();
		
		
		emf.close();

	}

}
