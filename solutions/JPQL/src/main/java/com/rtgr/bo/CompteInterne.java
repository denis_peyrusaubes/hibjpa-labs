package com.rtgr.bo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class CompteInterne {

	@Id
	@Column(name = "COMPTEINTERNE_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private java.lang.String numero;

	@Column(name = "DESCR")
	private java.lang.String description;

	@OneToMany(cascade = { CascadeType.ALL })
	@JoinColumn(name = "FK_COMPTEINTERNE")
	private List<Operation> operations = new ArrayList<Operation>();

	private double solde;

	@ManyToMany
	@JoinTable(name = "CLIENT_COMPTE_INTERNE", joinColumns = @JoinColumn(name = "FK_COMPTE_INTERNE", referencedColumnName = "COMPTEINTERNE_ID"), inverseJoinColumns = @JoinColumn(name = "FK_CLIENT", referencedColumnName = "ID"))
	private List<Client> titulaires = new ArrayList<Client>();

	public CompteInterne(String numero, String description, double solde) {
		this.numero = numero;
		this.description = description;
		this.solde = solde;
	}

	public CompteInterne() {
	}

	public void addOperation(Operation o) {
		this.operations.add(o);
	}

	public void addTitulaire(Client c) {
		titulaires.add(c);
		c.getCompteInternes().add(this);
	}

	/*
	 * Accesseurs
	 */

	public List<Operation> getOperations() {
		return operations;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}

	public List<Client> getTitulaires() {
		return titulaires;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(java.lang.String numero) {
		this.numero = numero;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(java.lang.String description) {
		this.description = description;
	}

	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	public void setId(java.lang.Long id) {
		this.id = id;
	}

	public java.lang.Long getId() {
		return id;
	}

}
