package com.rtgr.main;

import javax.persistence.EntityManager;

import org.junit.Assert;

import com.rtgr.utils.JPAUtil;

public class Main {

	public static void main(String[] args) {

		EntityManager entityManager = null;

		try {
			entityManager = JPAUtil.getEntityManager();
			entityManager.getTransaction().begin();

			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityManager.getTransaction().rollback();
			Assert.fail();
		} finally {
			entityManager.close();
		}
	}

	
	
	
}
