package com.rtgr.test;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;

import org.junit.Assert;
import org.junit.Test;

import com.rtgr.bo.Adresse;
import com.rtgr.bo.Agence;
import com.rtgr.bo.Client;
import com.rtgr.bo.CompteExterne;
import com.rtgr.bo.CompteInterne;
import com.rtgr.bo.Conseiller;
import com.rtgr.bo.Operation;
import com.rtgr.utils.JPAUtil;

public class Banque {

	private Long agenceId = null;

	// STEP 4
	@Test
	public void testCreateBanqueAvance() {
		EntityManager entityManager = null;

		try {
			entityManager = JPAUtil.getEntityManager();
			entityManager.getTransaction().begin();

			Adresse ad1 = new Adresse("22", "rue de Paris", "Paris", "75000");
			Adresse ad2 = new Adresse("22", "rue de la D�fense", "La D�fense",
					"92000");

			Client client1 = new Client("Winston", "Charlie");
			Client client2 = new Client("Perry", "Katty");
			Client client3 = new Client("Aznavour", "Charles");

			client1.addCompteExterne(createCompteExterne());
			client1.addCompteExterne(createCompteExterne());
			client1.addCompteExterne(createCompteExterne());

			client2.addCompteExterne(createCompteExterne());
			client2.addCompteExterne(createCompteExterne());
			client2.addCompteExterne(createCompteExterne());
			client2.addCompteExterne(createCompteExterne());

			client3.addCompteExterne(createCompteExterne());
			client3.addCompteExterne(createCompteExterne());

			CompteInterne ci1 = createCompteInterne("Compte Courant");
			client1.addCompteInterne(ci1);

			CompteInterne ci2 = createCompteInterne("Compte Epargne");
			client1.addCompteInterne(ci2);

			CompteInterne ci3 = createCompteInterne("Compte Joint");
			client2.addCompteInterne(ci3);
			client3.addCompteInterne(ci3);

			CompteInterne ci4 = createCompteInterne("Compte Epargne");
			client3.addCompteInterne(ci4);

			CompteInterne ci5 = createCompteInterne("Compte Courant");
			client3.addCompteInterne(ci5);

			CompteInterne ci6 = createCompteInterne("Compte Courant");
			client2.addCompteInterne(ci6);

			client1.setAdresse(ad1);
			client2.setAdresse(ad2);
			client3.setAdresse(ad1);

			Agence a = new Agence("Agence des peupliers");
			a.setAdresse(ad1);
			a.addClient(client1);
			a.addClient(client2);
			a.addClient(client3);
			a.addConseiller(createConseiller());
			a.addConseiller(createConseiller());
			a.addConseiller(createConseiller());
			a.addConseiller(createConseiller());

			entityManager.persist(a);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			entityManager.getTransaction().rollback();
			Assert.fail();
		} finally {
			entityManager.close();
		}

	}

	private int getNumber() {

		return ((int) (Math.random() * 100));
	}

	private Date createDate() {
		Date result;
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(GregorianCalendar.DAY_OF_YEAR, ((int) (Math.random() * 365) + 1));
		gc.set(GregorianCalendar.YEAR, 2000 + (((int) (Math.random() * 8) + 1)));
		result = gc.getTime();
		return result;
	}

	private Conseiller createConseiller() {
		return new Conseiller("nom" + getNumber(), "prenom" + getNumber());
	}

	private CompteExterne createCompteExterne() {
		return new CompteExterne("Beneficiaire" + getNumber(), "rib" + getNumber());
	}

	private CompteInterne createCompteInterne(String description) {
		CompteInterne result = new CompteInterne("num" + getNumber(), description, getNumber() * 20);
		int n = getNumber();
		Operation o;
		for (int i = 0; i < n; i++) {
			o = new Operation(createDate(), "Boucher", 12.78);
			result.addOperation(o);
		}

		return result;
	}

	private Agence findAgenceById(long id) {
		EntityManager manager = JPAUtil.getEntityManager();
		Agence a = manager.find(Agence.class, id);
		return a;
	}
}
