package com.rtgr.bo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Agence {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToMany(mappedBy = "agence", cascade = { CascadeType.ALL })
	private List<Client> clients = new ArrayList<Client>();

	// @Fetch(FetchMode.JOIN)
	@OneToMany(mappedBy = "agence", cascade = { CascadeType.ALL })
	// , fetch = FetchType.EAGER)
	private List<Conseiller> conseillers = new ArrayList<Conseiller>();

	private String nom;

	@Embedded
	private Adresse adresse;

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public Agence(String nom) {
		this.nom = nom;
	}

	protected Agence() {
	}

	public void addClient(Client c) {
		this.clients.add(c);
		c.setAgence(this);
	}

	public void addConseiller(Conseiller b) {
		this.conseillers.add(b);
		b.setAgence(this);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Client> getClients() {
		return clients;
	}

	public List<Conseiller> getConseillers() {
		return conseillers;
	}

}
