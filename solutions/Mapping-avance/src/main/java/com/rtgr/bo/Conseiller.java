package com.rtgr.bo;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("Conseiller")
public class Conseiller extends Personne {
	
	@ManyToOne (cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	@JoinColumn(name="FK_AGENCE")
	private Agence agence;

	public Conseiller(String nom,String prenom) {
		super(nom,prenom);
	}

	protected Conseiller() {
	}

	public Agence getAgence() {
		return agence;
	}

	public void setAgence(Agence agence) {
		this.agence = agence;
	}	
}
