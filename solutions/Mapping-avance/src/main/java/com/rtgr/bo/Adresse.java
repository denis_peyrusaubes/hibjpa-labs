package com.rtgr.bo;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Adresse {

	@Column(name = "NUM_RUE")
	private String numero;

	@Column(name = "CODE_POSTAL")
	private String codePostal;

	private String rue;

	private String ville;

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	// Constructeurs

	protected Adresse() {
	}

	public Adresse(String numero, String rue, String ville, String codePostal) {
		this.numero = numero;
		this.codePostal = codePostal;
		this.rue = rue;
		this.ville = ville;
	}

}
