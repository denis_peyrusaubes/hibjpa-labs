package com.rtgr.bo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("Client")
public class Client extends Personne {

	@ManyToMany(mappedBy = "titulaires", cascade = { CascadeType.ALL })
	private List<CompteInterne> compteInternes = new ArrayList<CompteInterne>();

	@OneToMany(mappedBy = "client", cascade = CascadeType.ALL)
	private List<CompteExterne> compteExternes = new ArrayList<CompteExterne>();

	@ManyToOne
	@JoinColumn(name = "FK_AGENCE")
	private Agence agence;


	@Embedded
	private Adresse adresse;

	public Client(String nom, String prenom) {
		super(nom,prenom);
	}

	protected Client() {
	}

	public void addCompteInterne(CompteInterne ci) {
		this.compteInternes.add(ci);
		ci.getTitulaires().add(this);
	}

	public void addCompteExterne(CompteExterne ce) {
		this.compteExternes.add(ce);
		ce.setClient(this);
	}


	public Agence getAgence() {
		return agence;
	}

	public void setAgence(Agence agence) {
		this.agence = agence;
	}

	protected List<CompteInterne> getCompteInternes() {
		return compteInternes;
	}

	public List<CompteExterne> getCompteExternes() {
		return compteExternes;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

}
