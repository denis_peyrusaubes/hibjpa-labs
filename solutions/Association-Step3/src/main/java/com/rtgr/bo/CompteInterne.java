package com.rtgr.bo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class CompteInterne {

	@Id
	@Column(name = "COMPTEINTERNE_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private java.lang.String numero;

	@Column(name = "DESCR")
	private java.lang.String description;

	private double solde;
	
	@ManyToOne
	private Client titulaire;
	
	@OneToMany(cascade=CascadeType.PERSIST,mappedBy="compteinterne")
	private List<Operation> operations = new ArrayList<Operation>();
	
	
	public CompteInterne() {
	}

	public CompteInterne(String numero, String description, double solde) {
		this.numero = numero;
		this.description = description;
		this.solde = solde;
	}
	
	public java.lang.Long getId() {
		return id;
	}

	public void setId(java.lang.Long id) {
		this.id = id;
	}

	
	public Client getTitulaire() {
		return titulaire;
	}

	public void setTitulaire(Client titulaire) {
		this.titulaire = titulaire;
	}
	
	public String getNumero() {
		return numero;
	}

	public void setNumero(java.lang.String numero) {
		this.numero = numero;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(java.lang.String description) {
		this.description = description;
	}

	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	public List<Operation> getOperations() {
		return operations;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}
	
	public void addOperation(Operation o) {
		operations.add(o);
	}

}
