package com.rtgr.bo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToMany(mappedBy = "client", cascade = CascadeType.ALL)
	private List<CompteExterne> compteExternes = new ArrayList<CompteExterne>();

	@ManyToOne
	@JoinColumn(name = "FK_AGENCE")
	private Agence agence;

	private String nom;
	private String prenom;

	public Client(String nom, String prenom) {
		this.nom = nom;
		this.prenom = prenom;
	}

	protected Client() {
	}


	public void addCompteExterne(CompteExterne ce) {
		this.compteExternes.add(ce);
		ce.setClient(this);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Agence getAgence() {
		return agence;
	}

	public void setAgence(Agence agence) {
		this.agence = agence;
	}

	public List<CompteExterne> getCompteExternes() {
		return compteExternes;
	}

}
