package com.rtgr.bo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Client {
	private Long id;
	private String nom;
	private String prenom;

	private List<CompteInterne> comptesInternes = new ArrayList<CompteInterne>();

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public void addCompteInterne(CompteInterne c) {
		comptesInternes.add(c);
	}
	
	@OneToMany(cascade={CascadeType.PERSIST},mappedBy="titulaire")
	public List<CompteInterne> getComptesInternes() {
		return comptesInternes;
	}

	public void setComptesInternes(List<CompteInterne> comptesInternes) {
		this.comptesInternes = comptesInternes;
	}
}
