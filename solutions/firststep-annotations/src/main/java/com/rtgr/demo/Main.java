package com.rtgr.demo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.rtgr.bo.Client;
import com.rtgr.bo.CompteInterne;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("RTGR_PU");

		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();


		CompteInterne compte1 = new CompteInterne();
		compte1.setDescription("nouveau Compte");
		compte1.setSolde(40000);
		CompteInterne compte2 = new CompteInterne();
		compte2.setDescription("Ancien Compte");
		compte1.setSolde(100);
		
		Client c = new Client();
		c.setNom("Durand");
		c.setPrenom("Paul");
		
		c.addCompteInterne(compte1);
		c.addCompteInterne(compte2);
		
			em.persist(c);

		em.getTransaction().commit();
		em.close();
		
		
		emf.close();

	}

}
