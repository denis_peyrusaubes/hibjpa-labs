package com.rtgr.bo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Agence {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToMany(mappedBy = "agence", cascade = { CascadeType.ALL })
	private List<Conseiller> conseillers = new ArrayList<Conseiller>();
	
	private String nom;

	public Agence(String nom) {
		this.nom = nom;
	}

	protected Agence() {
	}

	

	public void addConseiller(Conseiller b) {
		this.conseillers.add(b);
		b.setAgence(this);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setConseillers(List<Conseiller> conseillers) {
		this.conseillers = conseillers;
	}

	public List<Conseiller> getConseillers() {
		return conseillers;
	}

}
