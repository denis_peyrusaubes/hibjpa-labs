/**
 * 
 */
package com.rtgr.starter.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CompteInterne {
	@Id
	@Column(name = "COMPTEINTERNE_ID")
	private Long id;

	private String numero;
	
	@Column(name = "DESCR")
	private String description;

	private double solde;

	public CompteInterne() {
	}

	public CompteInterne(String numero, String description, double solde) {
		super();
		this.numero = numero;
		this.description = description;
		this.solde = solde;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

}
