package com.rtgr.starter.test;

import com.rtgr.starter.bo.CompteInterne;
import com.rtgr.starter.utils.JPAUtil;
import org.junit.Assert;

import javax.persistence.EntityManager;


/**
 * Created by denispeyrusaubes on 29/05/2016.
 */
public class Main {
    public static void main(String args[]) {
        EntityManager entityManager = null;

        try {

            entityManager = JPAUtil.getEntityManager();
            entityManager.getTransaction().begin();
            CompteInterne ci = new CompteInterne("5550", "Compte Courant", 1000);
            ci.setId(new Long(1));
            entityManager.persist(ci);

            entityManager.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();
            if (entityManager!= null) {
                entityManager.getTransaction().rollback();
            }
            Assert.fail();
        } finally {
            if (entityManager!= null) {
                entityManager.close();
            }
        }
    }
}
